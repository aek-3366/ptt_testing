import React from "react";
import Form from "../components/Form";
import TableUser from "../components/TableUser";
import TableAdmin from "../components/TableAdmin";

function SystemAdmission() {
  return (
    <div className="w-full">
      <div className="bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500 w-full h-full  p-8">
        <div className="flex flex-col justify-center items-center">
          <div className="text-2xl font-bold text-white">
            ระบบลงทะเบียนเข้างาน
          </div>
          <Form />
          <div className="pt-[3rem] w-full h-full">
            <TableUser />
          </div>
          <div className="pt-[3rem] w-full h-full">
            <div className="pb-4 font-bold text-white"> ระบบหลังบ้าน</div>
            <TableAdmin />
          </div>
        </div>
      </div>
    </div>
  );
}

export default SystemAdmission;
