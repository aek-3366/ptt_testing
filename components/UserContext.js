import React, { createContext, useContext, useState } from "react";

const UserContext = createContext();

export const UserProvider = ({ children }) => {
  const [userData, setUserData] = useState([]);
  
  const addUser = (user) => {
    setUserData([...userData, user]);
  };

  return (
    <UserContext.Provider value={{ userData, addUser }}>
      {children}
    </UserContext.Provider>
  );
};

export const useUser = () => useContext(UserContext);
