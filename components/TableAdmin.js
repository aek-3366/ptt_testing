
import React, { useState } from "react";
import { useUser } from "./UserContext";
import { Table, Input, Button, Space, Modal } from "antd";
import { SearchOutlined, EditOutlined } from "@ant-design/icons";
import Highlighter from "react-highlight-words";

const TableAdmin = ({ number }) => {
  const { userData } = useUser();
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const [sortedInfo, setSortedInfo] = useState({});
  const [showModal, setShowModal] = useState(false);
  const [chair, setChair] = useState();

  const seats = Array.from({ length: 20 }, (_, index) => index + 1);

  const columns = [
    {
      title: "ชื่อ",
      dataIndex: "name",
      key: "name",
      id: "0",
      sorter: (a, b) => a.name.localeCompare(b.name),
      sortOrder: sortedInfo.columnKey === "name" && sortedInfo.order,
    },
    {
      title: "นามสกุล",
      dataIndex: "lastName",
      key: "lastName",
      id: "1",
      sorter: (a, b) => a.lastName.localeCompare(b.lastName),
      sortOrder: sortedInfo.columnKey === "lastName" && sortedInfo.order,
    },
    {
      title: "เบอร์โทร",
      dataIndex: "phoneNumber",
      key: "phoneNumber",
      id: "2",
      sorter: (a, b) => a.phoneNumber.localeCompare(b.phoneNumber),
      sortOrder: sortedInfo.columnKey === "phoneNumber" && sortedInfo.order,
    },
    {
      title: "Action",
      id: "3",
      render: (record, index) => (
        <>
          <div
            className="bg-[#FE5000] p-4 flex justify-center items-center w-fit h-fit rounded-lg cursor-pointer"
            onClick={() => handleEdit(record, index)}
          >
            <EditOutlined style={{ color: "#FFFFFF" }} />
          </div>
        </>
      ),
    },
  ];
  const opacityValue = userData.length;

  const handleEdit = (record, index) => {
    setShowModal(true);
    setChair(record);
  };
  const handleClose = () => {
    setShowModal(false);
  };
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            // type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) => {
      return record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase());
    },
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => document.getElementById("search-input").select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  const handleChange = (pagination, filters, sorter) => {
    setSortedInfo(sorter);
  };

  return (
    <div className="w-full h-full">
      <Table
        columns={columns.map((col) => {
          if (
            col.dataIndex === "name" ||
            col.dataIndex === "lastName" ||
            col.dataIndex === "phoneNumber"
          ) {
            return {
              ...col,
              ...getColumnSearchProps(col.dataIndex),
            };
          }
          return col;
        })}
        dataSource={userData}
        onChange={handleChange}
        scroll={{
          x: 500,
          y: 300,
        }}
      />
      <Modal
        open={showModal}
        onCancel={handleClose}
        onOk={handleClose}
        footer={
          <div
            className="p-2 m-1 bg-green-600 rounded-lg text-white flex justify-center cursor-pointer"
            onClick={handleClose}
          >
            ยืนยัน
          </div>
        }
      >
        <div className="p-4">
          <div className="w-full flex justify-center pb-4 font-bold text-2xl">
            ผังที่นั่ง
          </div>
          <div className="grid grid-cols-4">
            {seats.map((seat, index) => (
              <div
              className={`${
                index < opacityValue ? "opacity-[46%]" : ""
              } p-2 m-1 bg-[#FE5000] rounded-lg text-white flex justify-center cursor-pointer`}
            >
              A {seat}
            </div>
            ))}
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default TableAdmin;
