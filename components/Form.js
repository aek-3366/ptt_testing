import React, { useState } from "react";
import { Input, Button } from "antd";
import { useUser } from "./UserContext";
import {  EditOutlined } from "@ant-design/icons";

const Form = () => {
  const [name, setName] = useState("");
  const [lastName, setLastName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const { addUser } = useUser();
const imageURL = <EditOutlined style={{color: 'red'}} size="medium"/>

  const register = () => {
    if (name === "" || lastName === "" || phoneNumber === "") {
      alert("กรุณากรอกข้อมุลให้ครบ");
      return;
    }
    const user = {
      name,
      lastName,
      phoneNumber,
    };
  
    addUser(user);
    setName("");
    setLastName("");
    setPhoneNumber("");
  };
  
  return (
    <div className="w-full h-full pt-4">
      <div className="flex  justify-center">
        <div className="flex flex-col gap-4 w-full">
          <div className="flex flex-col md:flex-row justify-center gap-4 md:pl-8 items-center">
            <div className="text-sm text-white md:w-fit w-full md:text-start ">
              ชื่อ
            </div>
            <Input
              id="search-input"
              className="rounded-xl h-[3rem] md:w-[20rem] w-full"
              value={name}
              status={!name ? "error" :null}
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <div className="flex flex-col md:flex-row justify-center gap-4 items-center">
            <div className="text-sm text-white md:w-fit w-full md:text-start ">
              นามสกุล
            </div>
            <Input
              className="rounded-xl h-[3rem] md:w-[20rem] w-full"
              value={lastName}
              status={!lastName ? "error" :null}
              onChange={(e) => setLastName(e.target.value)}
            />
          </div>
          <div className="flex flex-col md:flex-row justify-center gap-4 items-center">
            <div className="text-sm text-white md:w-fit w-full md:text-start ">
              เบอร์โทร
            </div>
            <Input
              className="rounded-xl h-[3rem] md:w-[20rem] w-full"
              value={phoneNumber}
              status={!phoneNumber ? "error" :null}
              onChange={(e) => setPhoneNumber(e.target.value)}
              maxLength={10}
            />
          </div>
          <div className="flex justify-center md:pl-14">
            <Button
              className="md:w-[20rem] w-full bg-gradient-to-r from-indigo-500 from-10% via-sky-500 via-30% to-emerald-500 to-90%"
              size="medium"
              onClick={register}
            >
              <div className="text-white">ลงทะเบียน</div>
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Form;
